﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            bool loop = true;
            string str;
            

            bool loopFruit = true;
            List<Fruit>  fruits = new List<Fruit>(); // Lista de frutas o etc
            Fruit fruit = null;

            while (loop)
            {
                while (loopFruit) // bucle repetitivo
                {

                    Console.WriteLine("Crear Fruta");
                    Console.WriteLine("1 - fresa sola");
                    Console.WriteLine("2 - fresa con platano");
                    Console.WriteLine("3 - Naranja");
                    Console.WriteLine("0 - Ver frutas");

                    str = Console.ReadLine();
                    switch (str)
                    {
                        case "1":
                            fruits .Add (new Fruit("Fresa"));
                            break;

                        case "2":
                            fruits .Add (new Fruit("Platano"));
                            break;

                        case "3":
                            fruits .Add (new Fruit("Naranja"));
                            break;

                        case "0":
                            Console.Clear(); 
                            foreach (Fruit f in fruits)
                                Console.WriteLine(f.name);
                            break;
                                 
                    }

                    Console.WriteLine("Continuar? (S/N)");
                    str = Console.ReadLine();
                    if (str == "S")
                        loopFruit = false;

                    Console.Clear(); //LIMPIA LOS BUCLES


                }





                Console.WriteLine("Preparar jugos");
                Console.WriteLine("1 - fresa sola");
                Console.WriteLine("2 - fresa con platano");
                Console.WriteLine("3 - Naranja");
                Console.WriteLine("4 - Ensalada de frutas");
                Console.WriteLine("0 - salir");

                 str = Console.ReadLine();
                switch (str)
                {
                    case "1":
                        fruit = fruits.Find((f) => f.name.ToLower() == "Fresa");
                        fruits.Remove(fruit);

                        Recipe.OnlyStrawberry(fruit);
                        break;

                    case "2":
                        Fruit fruit1 = fruits.Find((f) => f.name.ToLower() == "fresa");
                        Fruit fruit2 = fruits.Find((f) => f.name.ToLower() == "platano");
                        fruits.Remove(fruit1);
                        fruits.Remove(fruit2);

                      
                        Recipe.StrawberryWithBanana(fruit1, fruit2);
                        break;

                    case "3":

                        for (int i =0;i<fruits.Count;i++)
                        {
                            if (fruits[i].name.ToLower() == "Naranja")
                                fruit = fruits[i];
                        }
                        fruits.Remove(fruit);

                        Recipe.OnlyOrange(fruit);
                        break;

                    case "4":
                        Recipe salad = new Recipe(new string[] {"fresa","platano","platano" });

                        Recipe.Salad(salad, fruits);

                        break;

                    case "0":
                        loop = false;
                        break;
                }


                Console.WriteLine("Salir? (S/N)");
                str = Console.ReadLine();
                if (str == "S")
                    loop = false;

                Console.Clear();
            }

        }
    }
}
