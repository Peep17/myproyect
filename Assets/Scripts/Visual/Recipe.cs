﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ConsoleApp4
{
    class Recipe
    {
        static public void OnlyStrawberry(Fruit fruit)
        {
            if (fruit == null)
                Debug.Log("Error");

            if (fruit.name.ToLower() == "fresa")
                Debug.Log("Jugo de fresa");
        }

        static public void StrawberryWithBanana(Fruit strawberry, Fruit banana)
        {
            if (strawberry == null || banana == null)
                Debug.Log("Error");

            if (strawberry.name.ToLower() == "fresa" && banana.name.ToLower() == "platano")
                Debug.Log("Jugo de fresa con platano");

            else
                Debug.Log("Error");
        }

        static public void OnlyOrange(Fruit fruit)
        {
            if (fruit == null)
                Debug.Log("Error");

            if (fruit.name.ToLower() == "Naranja")
                Debug.Log("Jugo de Naranja");
        }


        static public void Salad(Recipe recipe, List<Fruit> fruits)
        {
            int count = 0;

            foreach (string fruitRequired in recipe.fruits) 
            {
                Fruit fruit = fruits.Find((f) => f.name.ToLower() == fruitRequired.ToLower());
                if (fruit != null) 
                {
                    fruits.Remove(fruit);
                    count++;
                }
            }

            if (count == recipe.fruits.Length)
                Debug.Log("Ensalada lista");
            else
                Debug.Log("Faltan ingredientes");
        }



        public string[] fruits;

        public Recipe(string[] fruits) 
            {
             this.fruits = fruits;
            }







    }
}
